package bs

import "testing"

func TestBinarySearch(t *testing.T) {
	nums := []int{1, 3, 5, 7, 9, 11, 12, 13}
	v := BinarySearch(nums, 7)
	if v != 3 {
		t.Errorf("期望结果：3,给定的结果：%d", v)
		return
	}

	v1 := BinarySearch(nums, 9)
	if v1 != 4 {
		t.Errorf("期望结果：4,给定的结果：%d", v1)
		return
	}

	v2 := BinarySearch(nums, 2)
	if v2 != -1 {
		t.Errorf("期望结果：-1,给定的结果：%d", v2)
		return
	}
}
