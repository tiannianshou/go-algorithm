package bs

// BinarySearch 二分查找
func BinarySearch(nums []int, n int) int {
	low := 0
	high := len(nums) - 1

	for low <= high {
		mid := (high + low) / 2
		guess := nums[mid]
		if guess == n {
			return mid
		} else if guess > n {
			high = mid - 1
		} else {
			low = mid + 1
		}
	}

	return -1
}
